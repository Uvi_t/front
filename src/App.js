import React, { useEffect } from 'react'
import { Route, useLocation } from 'react-router-dom'
import Header from './components/header/header'
import Footer from './components/footer/footer'
import Landing from './components/landing/landing'

function App() {
  //this is a new hook from react router that have multiple properties like pathname, hash, search or state etc
  const { pathname } = useLocation()

  //in this useEffect after every change in the pathname the page will scroll to top to simulate a page reload while moving from page to page
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return (
    <div>
      {pathname !== "/" ? <Header /> : null}
      <main>
        <Route path="/">
          <Landing />
        </Route>
      </main>
      {pathname !== "/" ? <Footer /> : null}
    </div>
  )
}

export default App
