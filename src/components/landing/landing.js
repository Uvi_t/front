import React from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { handleLang } from '../../redux/actions/lang'
import useLocale from '../../lang/useLocale'

function Landing() {
    //the translation Function is called getLocale
    const { getLocale } = useLocale()
    //Here i am intializing the new dispatch hook from react-redux
    const dispatch = useDispatch()
    function changeLang(e) {
        //here i am using the intialized dispatch hook to call actions from redux
        dispatch(handleLang(e.target.value))
    }
    return (
        <div>
            {/* in the next p element i passed a string to the getLocale function to handle the translation if the string is not defined in the ar.json and en.json then the word will return as is */}
            <p>{getLocale('test')}</p>
            {/* you can use the getLocale function to translate text in attributes like in the next placeholder */}
            <input placeholder={getLocale('test')} />
            <select onChange={changeLang}>
                <option value='en'>en</option>
                <option value='ar'>ar</option>
            </select>
            This is Landing
        </div>
    )
}

export default Landing