import { CHANGE_LANG } from '../actions/shared'

export default function lang(state = 'en', action) {
    switch (action.type) {
        case CHANGE_LANG:
            return action.lang
        default:
            return state
    }
}